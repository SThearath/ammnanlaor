export {nav} from './nav';
export {posts as POSTS} from './post';

export const sliders = [
    {
        id: "a",
        src: "https://external-content.duckduckgo.com/iu/?u=http%3A%2F%2Fwowslider.com%2Fsliders%2Fdemo-91%2Fdata1%2Fimages%2Fstream384655_1280.jpg&f=1&nofb=1",
        altText: "test",
        title: "Hello World 1",
        text: "blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah"
    },
    {
        id: "b",
        src: "https://external-content.duckduckgo.com/iu/?u=http%3A%2F%2Fwowslider.com%2Fsliders%2Fdemo-91%2Fdata1%2Fimages%2Fstream384655_1280.jpg&f=1&nofb=1",
        altText: "test",
        title: "Hello World 2",
        text: "blah blah blah"
    },
    {
        id: "c",
        src: "https://external-content.duckduckgo.com/iu/?u=http%3A%2F%2Fwowslider.com%2Fsliders%2Fdemo-91%2Fdata1%2Fimages%2Fstream384655_1280.jpg&f=1&nofb=1",
        altText: "test",
        title: "Hello World 3",
        text: "blah blah blah"
    }
];

export const topic = {
    title: "Hello World 2",
    text: "blah blah blah"
};