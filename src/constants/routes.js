import CreatePostPage from '../views/pages/CreatePost';
import HomePage from '../views/pages/Home';
import ForumPage from '../views/pages/Forum';
import ProfilePage from '../views/pages/Profile';
import NotFoundPage from '../views/pages/NotFound';
import LoginPage from '../views/pages/Login';
import RegisterPage from '../views/pages/Register';
import ForumQuestionDetailsPage from '../views/pages/ForumQuestionDetails';
import PostByTopicPage from "../views/pages/PostByTopic";


export default [
    {
        path: '/login',
        component: LoginPage
    },
    {
        path: '/register',
        component: RegisterPage
    },
    {
        path: '/home',
        component: HomePage
    },
    {
        path: '/by-topics/:topicId',
        component: PostByTopicPage
    },
    {
        path: '/profile',
        component: ProfilePage
    },
    {
        path: '/:topicId/create-post',
        component: CreatePostPage
    },
    {
        path: '/forum',
        component: ForumPage
    },
    {
        path: '/forum-details/:forumId',
        component: ForumQuestionDetailsPage
    },
    /* Fallback Route - Must be kept at the bottom */
    {
        path: '*',
        component: NotFoundPage
    }
];