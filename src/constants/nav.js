export const nav = [
    {
        link: "/",
        title: "ទំព័រដើម"
    },
    {
        links: [
            {
                link: "/by-topics/5e348d012e78a7eb189c4745",
                title: "វិទ្យាសាស្ត្រ"
            },
            {
                link: "/by-topics/5e348cf12e78a7eb189c4744",
                title: "ប្រវត្តិវិទ្យា"
            }
        ],
        title: "ប្រធានបទ"
    },
    {
        link: "/forum",
        title: "ចម្ងល់"
    },
    // {
    //     links: [
    //         {
    //             link: '/profile/',
    //             title: 'ព័ត៌មាន​ទូទៅ',
    //             href:'#aboutme'
    //         },
    //         {
    //             link: '/profile/',
    //             title: 'ផុសរបស់ខ្ញុំ',
    //             href:'#mypost'
    //         },
    //         {
    //             link: '#:3',
    //             title: 'ចាកចេញ'
    //         }
    //     ],
    //     title: "ខ្ញុំ"
    // }
];