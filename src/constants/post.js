export const posts = [
    {
        id: "aaa",
        title: "របបសង្គមរាស្ត្រនិយម 1",
        subject: "ប្រវត្តិសាស្ត្រ",
        postedBy: "ខ្ញុំ 1",
        postedOn: "នៅ ០៨ កញ្ញា ២០១៩",
        content: "1. Lorem ipsum dolor sit amet, consectetur adipisicing elit. A atque culpa, cum delectus dignissimos doloribus esse fugiat hic illo in laudantium magni obcaecati officia quae quasi qui repudiandae sunt veritatis!",
        views: "2000",
        comments: "1"
    },
    {
        id: "bbb",
        title: "របបសង្គមរាស្ត្រនិយម 2",
        subject: "ប្រវត្តិសាស្ត្រ",
        postedBy: "ខ្ញុំ 2",
        postedOn: "នៅ ០៨ កញ្ញា ២០១៩",
        content: "2. Lorem ipsum dolor sit amet, consectetur adipisicing elit. A atque culpa, cum delectus dignissimos doloribus esse fugiat hic illo in laudantium magni obcaecati officia quae quasi qui repudiandae sunt veritatis!",
        views: "2000",
        comments: "1"
    },
    {
        id: "ccc",
        title: "របបសង្គមរាស្ត្រនិយម 3",
        subject: "ប្រវត្តិសាស្ត្រ",
        postedBy: "ខ្ញុំ 3",
        postedOn: "នៅ ០៨ កញ្ញា ២០១៩",
        content: "3. Lorem ipsum dolor sit amet, consectetur adipisicing elit. A atque culpa, cum delectus dignissimos doloribus esse fugiat hic illo in laudantium magni obcaecati officia quae quasi qui repudiandae sunt veritatis!",
        views: "2000",
        comments: "1"
    }
];