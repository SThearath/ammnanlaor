import React, {Fragment} from 'react';

import {PostContent, TrendingListView} from './../../components';
import TopicBanner from "../../components/TopicBanner";
import {Button, Col, Container, Form, Input, Row} from "reactstrap";
import {Link} from "react-router-dom";
import PostsList from "../../components/posts/PostsList";
import ChangeTopics from "../../components/ChangeTopics";


const withPostLayout = Page => {
    return props => (
        <Fragment>
            <TopicBanner/>
            <Row className="pl-4 mt-4">
                <Col xs={12} md={8} xl={9}>
                    <div className="mb-5 mb-md-0 px-5">
                        <Page/>
                    </div>
                </Col>
                <Col xs={12} md={4} xl={3}>
                    <div className="px-4">
                        <TrendingListView/>
                    </div>
                </Col>
            </Row>
        </Fragment>
    );
};

export default withPostLayout;