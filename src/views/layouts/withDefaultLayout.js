import React from 'react';

import {Footer, Header} from './../../components';


const withDefaultLayout = Page => {
    return props => (
        <div className="layout d-flex flex-column">
            <Header {...props}/>
            <div className="flex-grow-1">
                <Page {...props}/>
            </div>
            <Footer {...props}/>
        </div>
    )
};

export default withDefaultLayout;