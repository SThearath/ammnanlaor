import React, {useEffect, useState} from 'react';
import {Link, useHistory, useRouteMatch} from 'react-router-dom'
import {Container,Row,Col,NavLink} from 'reactstrap';

import {Filter, QuestionCardView} from './../../components';
import {withDefaultLayout, withPostLayout} from './../layouts';
import PostQuestion from './PostQuestion';
import Axios from "axios";


const Forum = ()=>{
    const routeMatch = useRouteMatch();
    const routeHistory = useHistory()

    const redirect = url => {
        routeHistory.push(url);
    };

    const [state, setState] = useState({posts: []});

    const syncState = () => {
        Axios.get('http://localhost:5000/api/questions')
            .then(res => {
                setState({posts: res.data});
            }).catch(err => {
            console.log(err)
        })
    }


    useEffect(() => {
        syncState();
    }, []);

    return(
            <Container className="px-4 mt-4" fluid={true}>
            <Row>
                <Col xs={12} md={2} xl={2} className="mb-2">
                    <PostQuestion  syncQuestions={syncState}/>
                    <Filter />
                </Col>
                <Col className="pr-xl-4 mb-2">
                    <h5 className="mb-2 px-4 py-6 card-blue text-white text-center" >បណ្តុំនៃសំណួរ</h5>
                    <QuestionCardView {...state} redirect={redirect}/>
                </Col>
           </Row>
            </Container>
    )
};

export default  withDefaultLayout(Forum);