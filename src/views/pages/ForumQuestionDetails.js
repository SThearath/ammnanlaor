import React,{useState,useEffect} from 'react';
import Axios from 'axios';

import {useRouteMatch} from 'react-router-dom'
import {Container,ListGroup,ListGrouppost,NavLink,Input,InputGroup,InputGroupAddon,Button,Row,Col, ListGroupItem} from 'reactstrap';
import {Link} from 'react-router-dom';
import {QuestionDetailView, QuestionCardView} from './../../components';
import {withDefaultLayout, withPostLayout} from './../layouts';
import view from '../../assets/viewer_icon.svg'


const ForumQuestionDetails = ()=>{
    const routeMatch = useRouteMatch();
    const id = routeMatch.params.forumId;
    const [state, setState] = useState({posts: []});
    useEffect( () => {
        Axios.get('http://localhost:5000/api/questions/'+id)
            .then(res => {
                setState({posts: res.data})
            }).catch(err => {
                console.log(err)
            })
    }, []);
    
    return(
            <Container className="px-4 mt-4">
            <h3 className="mb-2 px-4 py-6 card-red text-white text-center" >សំណួរ និង ចម្លើយ </h3> 
            {/* Get Question Error */}
            {/* <div>
            {
                state.posts.map(post => (
                    <NavLink to={"/forum-details/"+post._id} tag={Link} className="card-white mb-1 ml-3 mr-3 text-dark p-0">
                    <div className="p-3">
                        <h4 className="mb-3 crop-text-2">{post.title}</h4>
                        {post.user}{post.topic}  ~ C:{post.createdAt}U:{post.updatedAt}
                        <label className="float-right">{post.views}</label>
                        <img className="iconView float-right" src={view} alt="ImageView"/>
                    </div>
                    </NavLink>
                ))
            }
            </div> */}
            <div className="p-2 card-white text-dark p-0 mb-2">
                        <h4 className="mb-4 crop-text-2">Title</h4>
                        Recsam  ~ 22/01/2020
                        <label className="float-right">100</label>
                        <img className="iconView float-right" src={view} alt="ImageView"/>
                    </div>
            <div> 
                 <div className="p-2 p-0 mb-5">
                        <InputGroup>
                        <Input />
                        <InputGroupAddon addonType="append">
                        <Button color="primary">ផ្តល់ចម្លើយ</Button>
                        </InputGroupAddon>
                        </InputGroup>
                    </div>
                    <QuestionDetailView key={id} id='id'/>
            </div>
            </Container>
    )
};

export default withDefaultLayout(ForumQuestionDetails);