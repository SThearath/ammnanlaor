import React from 'react';
import {Link} from 'react-router-dom';

import {withDefaultLayout} from './../layouts';


const NotFound = () => {
    return (
        <div className="d-flex flex-column align-items-center">
            <div className="text-center my-4 mx-2">
                <h1 className="mb-4">អូ! ទំព័រនេះ យើងមិនអាចរកឃើញទេ</h1>
                <h3>តោះ! ទៅទំព័រដើមវិញ</h3>
            </div>
            <Link to="/" className="btn btn-primary mb-5">ត្រឡប់ទៅទំព័រដើម</Link>
        </div>
    );
};

export default withDefaultLayout(NotFound);