import React, {Fragment, useEffect, useState} from 'react';

import withDefaultLayout from '../layouts/withDefaultLayout';
import TopicBanner from "../../components/TopicBanner";
import {Col, Container, Row} from "reactstrap";
import {TrendingListView} from "../../components";
import PostDetailContent from "../../components/posts/PostDetailContent";
import Axios from "axios";


const PostDetail = props => {
    const [post, setPost] = useState({
        name: ''
    });
    const [comments, setComments] = useState([]);

    useEffect(  () => {
        syncPost()
    }, []);

    useEffect(() => {
        syncComments()
    }, []);
    console.log(post)

    const syncPost = () => {
        Axios.get(`http://localhost:5000/api/posts/details/${props.match.params.postId}`)
            .then(res => {
                console.log(res.data)
                setPost(res.data);
            }).catch(err => {
            console.log(err)
        });
    }

    const syncComments = () => {
        Axios.get(`http://localhost:5000/api/posts/${props.match.params.postId}/comments`)
            .then(res => {
                console.log(res.data)
                setComments(res.data);
            }).catch(err => {
            console.log(err)
        });
    };

    return (
        <Fragment>
            <TopicBanner topicName={post.topic && post.topic.name}/>
            <Row className="pl-md-4 mt-4">
                <Col xs={12} md={8} xl={9}>
                    <div className="mb-5 mb-md-0 px-5">
                        <PostDetailContent {...props} post={post} comments={comments} syncComments={syncComments} />
                    </div>
                </Col>
                <Col xs={12} md={4} xl={3}>
                    <div className="px-4">
                        <TrendingListView/>
                    </div>
                </Col>
            </Row>
        </Fragment>
    )
};

export default withDefaultLayout(PostDetail);