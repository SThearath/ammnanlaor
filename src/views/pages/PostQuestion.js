import React, {useState} from 'react';
import Axios from 'axios'
import {
    Button, Modal, ModalHeader, ModalBody,
    ModalFooter, Form, Input, Label, FormGroup
} from 'reactstrap';
import Cookies from "js-cookie";
import {useForm} from 'react-hook-form';


const PostQuestion = (props) => {
    // Declare a new state variable, which we'll call "count"
    const {
        buttonLabel,
        className_
    } = props;

    const [modal, setModal] = useState(false);
    const toggle = () => setModal(!modal);
    const {register, handleSubmit, errors} = useForm();
    const onSubmit = values => {
        Axios.post('http://localhost:5000/api/questions', {
            title: values.question,
            topic: values.topic,
            user: "5e34d33c296fe41ee89d00af"
        }, {
            headers: {'Authorization': `Bearer ${Cookies.get('token')}`}
        }).then(res => {
            console.log(res);
            props.syncQuestions();
        }).catch(err => {
            console.log(err);
        });
    };
    console.log(errors);

    return (
        <div>
            <Button style={{height: 40, width: "100%", marginBottom: 10}} outline color="danger" onClick={toggle}> ឆ្ងល់
                ?</Button>
            <Modal isOpen={modal} toggle={toggle} className={className_}>
                <ModalHeader toggle={toggle}>បញ្ចេញចម្ងល់</ModalHeader>
                <ModalBody>
                    <Form onSubmit={handleSubmit(onSubmit)}>
                        <FormGroup>
                            <Label for="questionLabel">សំណួរ *</Label>
                            <Input
                                name="question"
                                type="textarea"
                                style={{height: 200, marginBottom: 10}}
                                placeholder="សូមវាយសំណួរនៅទីនេះ"
                                ref={register({required: true})}
                                innerRef={register({
                                    required: "សូមបញ្ចូលសំណួរ"
                                })}
                            />
                            {errors.question && <span style={{color: "red"}}>សូមបញ្ចូលសំណួរ</span>}
                        </FormGroup>
                        <FormGroup>
                            <Label for="topicLable">ជ្រើសរើស ប្រធានបទ *</Label>
                            <Input
                                name="topic"
                                type="select"
                                style={{height: 40, marginBottom: 10}}
                                ref={register({required: true})}
                                innerRef={register({
                                    required: ""
                                })}>
                                <option value="5e348ce02e78a7eb189c4743">អក្សរសាស្ត្</option>
                                <option value="5e348cf12e78a7eb189c4744">ប្រវត្តិសាស្ត្រ</option>
                                <option value="5e348d012e78a7eb189c4745">វិទ្យាសាស្ត្រ</option>
                                <option value="5e348d122e78a7eb189c4746">ការផ្សងព្រេង</option>
                            </Input>
                            {errors.topic && <span style={{color: "red"}}>សូមជ្រើសរើស</span>}
                        </FormGroup>
                        <FormGroup>
                            <Button className="float-right" color="primary" onClick={toggle}
                                    type="submit">បញ្ចេញសំណួរ</Button>{' '}
                            <Button className="float-right" color="light" onClick={toggle}>បិទ</Button>
                        </FormGroup>
                    </Form>
                </ModalBody>
                <ModalFooter>

                </ModalFooter>
            </Modal>
        </div>
    );

};

export default PostQuestion;