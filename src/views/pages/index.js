export {default as ForumPage} from './Forum';
export {default as HomePage} from'./Home';
export {default as NotFoundPage} from './NotFound';
export {default as PostByTopicPage} from './PostByTopic';
export {default as ForumQuestionDetailsPage} from './ForumQuestionDetails';
export {default as LoginPage} from './Login';
export {default as RegisterPage} from './Register';
