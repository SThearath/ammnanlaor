import React from 'react';
import Cookies from 'js-cookie';
import {Redirect} from 'react-router-dom';


const Logout = props => {
    Cookies.remove('token');
    Cookies.remove('userId');
    return (<Redirect to="/"/>);
};

export default Logout;