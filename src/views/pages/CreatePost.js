import React, {Fragment, useEffect, useState} from 'react';
import {Col, Row} from 'reactstrap';

import withDefaultLayout from "../layouts/withDefaultLayout";
import withPostLayout from "../layouts/withPostLayout";
import CreatePostForm from "../../components/forms/CreatePostForm";
import TopicBanner from "../../components/TopicBanner";
import {TrendingListView} from "../../components";
import Axios from "axios";


const CreatePost = props => {

    let [topic, setTopic] = useState({});

    useEffect( () => {
        Axios.get(`http://localhost:5000/api/topics/${props.match.params.topicId}`)
            .then(res => {
                console.log(res.data)
                setTopic(res.data);
            }).catch(err => {
            console.log(err)
        })
    }, []);

    return (
        <Fragment>
            <TopicBanner topicName={topic.name} />
            <Row className="pl-4 mt-4">
                <Col xs={12} md={8} xl={9}>
                    <div className="mb-5 mb-md-0 px-5">
                        <Fragment>
                            <h3 className="mb-4 pb-2" >បង្កើតផុសថ្មី</h3>
                            <CreatePostForm {...props} topic={topic}/>
                        </Fragment>
                    </div>
                </Col>
                <Col xs={12} md={4} xl={3}>
                    <div className="px-4">
                        <TrendingListView/>
                    </div>
                </Col>
            </Row>
        </Fragment>
    );
};

export default withDefaultLayout(CreatePost);