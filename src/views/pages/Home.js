import React, {useEffect, useState} from 'react';
import {Container} from 'reactstrap';

import {withDefaultLayout} from './../layouts';
import Banner from '../../components/home/Banner';
import TopicsList from '../../components/home/TopicsList';
import PopularPosts from '../../components/home/PopularPosts';
import TOPICS from '../../constants/topics';
import Axios from "axios";
import {useHistory, useRouteMatch} from "react-router-dom";


const Home = props => {
    const routeMatch = useRouteMatch();
    const routeHistory = useHistory()

    const redirect = url => {
        routeHistory.push(url);
    };

    const [popularPosts, setPopularPosts] = useState([]);

    useEffect(() => {
        syncPopularPosts();
    }, []);

    const syncPopularPosts = () => {
        Axios.get('http://localhost:5000/api/posts')
            .then(res => {
                setPopularPosts(res.data);
            })
            .catch(err => {
                console.log(err);
            });
    };

    return (
        <div className="p-0">
            <Banner/>
            <Container className="px-4 mt-4">
                <TopicsList topics={TOPICS}/>
                <PopularPosts posts={popularPosts} redirect={redirect}/>
            </Container>
        </div>
    );
};

export default withDefaultLayout(Home);