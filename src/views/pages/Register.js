import React, { useState } from 'react';
import {
  Col, Container, Row
} from 'reactstrap';
import {withDefaultLayout} from './../layouts';
import RegisterForm from '../../components/forms/RegisterForm';


const Login = ()=>{


    return(
        <Container className="px-4 mt-3 mb-2">
            <Row className="justify-content-center">
                <Col xs={12} md={8}><RegisterForm/></Col>
            </Row>
        </Container>
    );
}
export default withDefaultLayout(Login);