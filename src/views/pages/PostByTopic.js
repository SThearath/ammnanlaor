import React, {Fragment, useEffect, useState} from 'react';
import {Row, Col, Form, Input, Button} from 'reactstrap';
import {Link, useRouteMatch, useHistory} from 'react-router-dom';

import {withDefaultLayout} from './../layouts';
import TopicBanner from '../../components/TopicBanner';
import PostsList from '../../components/posts/PostsList';
import { TrendingListView } from '../../components';
import ChangeTopics from '../../components/ChangeTopics';
import Axios from "axios";
import Cookies from "js-cookie";
import PopularPosts from "../../components/home/PopularPosts";


const PostByTopic = props => {
    const routeMatch = useRouteMatch();
    const routeHistory = useHistory()


    const {topicId} = routeMatch.params;

    const redirect = url => {
        routeHistory.push(url);
    };

    let [topic, setTopic] = useState({
        name: 'ប្រធានបទ'
    });
    let [posts, setPosts] = useState([]);
    let [search, setSearch] = useState("");

    const onHandleChange = event => {
        setSearch(event.target.value);
    }

    const syncTopic = () => {
        Axios.get(`http://localhost:5000/api/topics/${props.match.params.topicId}`)
            .then(res => {
                setTopic(res.data);
            })
            .catch(err => {
                console.log(err);
            })
    }

    const syncPosts = () => {
        Axios.get(`http://localhost:5000/api/posts/${props.match.params.topicId}`)
            .then(res => {
                if (res.data.length)
                    setPosts(res.data);
                else
                    setPosts([]);
            })
            .catch(err => {
                console.log(err);
            })
    }

    const fetch = () => {
        Axios.get(`http://localhost:5000/api/posts/fetch?title=${search}`)
            .then(res => {
                console.log(res.data)
                setPosts(res.data)
            });
    }

    useEffect(() => {
        syncTopic();
    }, [topicId]);

    useEffect(  () => {
        if (!topic)
            return;
        syncPosts();
    }, [topic]);

    // UI Helper
    const getTopBar = () => {
        if (!Cookies.get('token')) {
            return (
                <div className="d-flex pb-2 mb-4"  >
                    <Form className="d-flex flex-grow-1 mr-4">
                        <Input type="text" value={search} onChange={onHandleChange}/>
                        <Button color="secondary" onClick={fetch}><i className="fas fa-search"/></Button>
                    </Form>
                </div>
            );
        }
        return (
            <div className="d-flex pb-2 mb-4"  >
                <Form className="d-flex flex-grow-1 mr-4">
                    <Input type="text" value={search} onChange={onHandleChange}/>
                    <Button color="secondary" onClick={fetch}><i className="fas fa-search"/></Button>
                </Form>
                <Link to={`/${props.match.params.topicId}/create-post`} className="btn btn-primary px-5">បង្កើតផុស</Link>
            </div>
        );
    };

    return (
        <Fragment>
            <TopicBanner {...props} topicName={topic ? topic.name : ''}/>
            <Row className="px-4 mt-4">
                <Col xs={12} md={8} xl={9} className="mb-3 mb-md-0 px-5">
                    {getTopBar()}
                    <PostsList {...props} posts={posts} redirect={redirect}/>
                </Col>
                <Col xs={12} md={4} xl={3}>
                    <div className="b-left px-4">
                        <ChangeTopics/>
                        <TrendingListView/>
                    </div>
                </Col>
            </Row>
        </Fragment>
    );
};

export default withDefaultLayout(PostByTopic);