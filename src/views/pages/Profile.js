import React from 'react';
import { Container, Row, Col} from 'reactstrap';

import { withDefaultLayout, withPostLayout } from './../layouts';
import ProfileFeature from '../../components/ProfileFeature';
import ProfileView from './../../components/ProfileView';
import MyPostProfile from '../../components/MyPostProfile';

const Profile = () => {
    return (
        <Container className="themed-container" fluid={true}>
                    <ProfileView/>
                    <MyPostProfile/>
        </Container>
    )
};


export default withDefaultLayout(Profile);