import React from 'react';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';

import './App.css';
import ROUTES from './constants/routes';
import HomePage from './views/pages/Home';
import PostDetail  from "./views/pages/PostDetail";
import ScrollToTop from "./ScrollToTop";
import Logout from "./views/pages/Logout";
import TextEditor from "./components/forms/TextEditor";



function App(props) {
    return (
        <div className="App">
          <Router>
              <ScrollToTop>
                  <Switch>
                      <Route exact path="/" render={props => <HomePage {...props}/>}/>
                      <Route path="/post-detail/:postId" component={PostDetail}/>
                      <Route path="/logout" component={Logout}/>
                      {
                         ROUTES.map(route => (
                             <Route path={route.path} render={props => <route.component {...props}/>}/>
                         ))
                      }

                  </Switch>
              </ScrollToTop>
          </Router>
        </div>
    );
}

export default App;
