import React, {useState} from 'react';

import Trending from './Trending';

const TrendingListView = () => {
    const [trendingPostList] = useState([
        [
            {id: "aaaa", title: 'popular link', link: '/by-topics/11'},
            {id: "aabb", title: 'popular link', link: '/by-topics/12'},
            {id: "aacc", title: 'popular link', link: '/by-topics/13'
        }],
        [
            {id: "bbaa", title: 'popular link', link: '/by-topics/21'},
            {id: "bbbb", title: 'popular link', link: '/by-topics/22'},
            {id: "bbcc", title: 'popular link', link: '/by-topics/23'
        }],
        [
            {id: "ccaa", title: 'popular link', link: '/by-topics/31'},
            {id: "ccbb", title: 'popular link', link: '/by-topics/32'},
            {id: "cccc", title: 'popular link', link: '/by-topics/33'
        }]
    ]);

    return (
        <div className="">
            <h4 className="mb-3 ml-md-3"><icon className="fas fa-pen-square mr-2"/>ប្រធានបទពេញនិយម</h4>
            {
                trendingPostList.map((trendingPost, index) => (
                    <Trending key={index} trendingPosts={trendingPost}/>
                ))
            }
        </div>
    );
};

export default TrendingListView;