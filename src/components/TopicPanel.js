import React from 'react';

import {topic} from './../constants';


const TopicPanel = () => {
    return (
        <div className="bg-carousel pt-3 p-md-3 d-flex flex-column flex-md-row text-lighter">
            <img className="carousel-img" src="https://external-content.duckduckgo.com/iu/?u=http%3A%2F%2Fwowslider.com%2Fsliders%2Fdemo-91%2Fdata1%2Fimages%2Fstream384655_1280.jpg&f=1&nofb=1" alt="hello"/>
            <div className="flex-grow-1 px-4">
                <h4 className="text-center">{topic.title}</h4>
                <p>{topic.text}</p>
            </div>
        </div>
    );
};

export default TopicPanel;