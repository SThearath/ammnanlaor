import React, {Fragment} from 'react';
import {Card,CardBody} from 'reactstrap';
import './../assets/profile_css/styleProfile.css';

const MyPostProfile = () => {
    return (
        <>
        <Card className="p-5 ml-1 mr-1" >
            <CardBody>
            <div class="container">
                <h1 class="page-section text-center text-black mb-0">ផុសរបសខ្ញុំ</h1>
                <div class="row">
                    <div class="col-md-5 col-lg-3">
                    <div class="portfolio-item mx-auto" data-toggle="modal" data-target="#portfolioModal1">
                        <a href="/hello">
                        <img class="img-fluid" src="https://support.overwolf.com/wp-content/uploads/2016/07/avatar-300x300.png" alt=""/>
                        </a>
                    </div>
                    </div>
                </div>
            </div>
        </CardBody>
    </Card>
        </>
    );
};

export default MyPostProfile;