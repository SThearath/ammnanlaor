import React from 'react';


const PostCardView = (props) => {
    const {title, content, postedBy, postedOn, subject, views, comments} = props.post;

    return (
        <div className="mt-3 d-flex flex-column flex-md-row-reverse">
            <div className="flex-grow-7 ml-3 mr-3 mr-md-5">
                <div className="card">
                    <h5 className="text-center">{title}</h5>
                    <p>{content}</p>
                </div>
            </div>
            <div className="flex-grow-1 text-right ml-md-5 mr-3 mr-md-0 inline-text">
                <p className="mt-2 mb-1 text-primary">{subject}</p>
                <p className="mb-1">ដោយ <strong>{postedBy}</strong></p>
                <p className="mb-1 text-faded">{postedOn}</p>
                <p className="mb-1">{views}  នាក់</p>
                <p className="mb-4 mb-md-3">{comments} មតិ</p>
            </div>
        </div>
    );
};

export default PostCardView;