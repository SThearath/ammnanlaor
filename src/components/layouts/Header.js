import React, {Fragment, useState} from 'react';
import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    NavbarText
} from 'reactstrap';
import {Link} from 'react-router-dom';

import Navigation from "./Navigation";


const Header = () => {
    const [isOpen, setIsOpen] = useState(false);
    const toggle = () => setIsOpen(!isOpen);

    // For render
    return (
        <Fragment>
            <Navbar light expand="md" className="bg sticky-top">
                <NavbarBrand to="/" tag={Link} className="text-primary">អំណានល្អ</NavbarBrand>
                <NavbarToggler onClick={toggle} />
                <Collapse isOpen={isOpen} navbar>
                    <Navigation/>
                </Collapse>
            </Navbar>
        </Fragment>
    );
};

export default Header;
