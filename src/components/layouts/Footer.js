import React from 'react';


const Footer = () => {
    return (
        <div className="d-flex flex-column flex-md-row footer py-4 mt-4 sticky-bottom" fluid={true}>
            <div className="mx-auto ml-md-3 mr-md-0">
                រក្សាសិទ្ធដោយ ក្រុមទី៤
            </div>
            <div className="mx-auto ml-md-4">
                ២០១៩​ ~ ២០៩៩
            </div>
            <div className="mx-auto mr-md-3 ml-md-auto">
                www.amnanlaor.com.kh
            </div>
        </div>
    );
};

export default Footer;