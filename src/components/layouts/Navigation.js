import React from 'react';
import {
    Collapse,
    DropdownItem,
    DropdownMenu,
    DropdownToggle,
    Nav,
    NavItem,
    NavLink,
    UncontrolledDropdown
} from "reactstrap";
import {Link} from 'react-router-dom';
import Cookies from 'js-cookie';

import {nav} from './../../constants';


const Navigation = (props) => {
    const {isOpen} = props;

    const showLink = (navLink) => {
        return (
            <NavItem key={navLink.link}>
                <NavLink to={navLink.link} tag={Link}>{navLink.title}</NavLink>
            </NavItem>
        );
    };

    const showDropdown = (navLink) => {
        return (
            <UncontrolledDropdown nav inNavbar key={navLink.title}>
                <DropdownToggle nav caret>
                    {navLink.title}
                </DropdownToggle>
                <DropdownMenu right className="bg-primary py-0">
                    {
                        navLink.links.map(link => {
                            return (
                                <DropdownItem key={link.link} className="py-2 text-dropdown" to={link.link} tag={Link}>{link.title}
                                </DropdownItem>
                            );
                        })
                    }
                </DropdownMenu>
            </UncontrolledDropdown>
        )
    };

    const showProfile = () => {
        if (!Cookies.get('token')) {
            return showLink({
                link: "/login",
                title: "ចូលគណនី"
            });
        }
        return showDropdown({
            links: [
                {
                    link: '/profile/',
                    title: 'ព័ត៌មាន​ទូទៅ',
                    href:'#aboutme'
                },
                {
                    link: '/profile/',
                    title: 'ផុសរបស់ខ្ញុំ',
                    href:'#mypost'
                },
                {
                    link: '/logout',
                    title: 'ចាកចេញ'
                }
            ],
                title: "ខ្ញុំ"
        });
    }

    return (
        <Nav navbar className="ml-auto">
            {
                nav.map(link => (link.links ? showDropdown(link) : showLink(link)))
            }
            {showProfile()}
        </Nav>
    );
};

export default Navigation;