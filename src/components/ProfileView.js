import React, {Fragment} from 'react';
import {Card,CardBody} from 'reactstrap';

import './../assets/profile_css/styleProfile.css'

const user = [
    {
        uid:"123",
        uName:"Mostqito Kheang",
        uDescription:"Graphic Artist - Web Designer - Illustrator"
    }
];

const ProfileView = () => {
    return (
    user.map(user =>(
        <>
        <Card className="p-5 ml-1 mr-1" id="aboutme">
            <CardBody>
            <div class="container d-flex align-items-center flex-column">
            <img class="img-fluid rounded-pill mb-5" src="https://support.overwolf.com/wp-content/uploads/2016/07/avatar-300x300.png" alt=""/>
            <h1 class="masthead-heading text-center text-black  mb-0 ">{user.uName}</h1>
            <br/>
            <h3 class="masthead-subheading text-center text-black mb-0 ">Description</h3>
            <p class="masthead-subheading text-center font-weight-light text-black mb-0">{user.uDescription}</p>
        </div>
            </CardBody>
        </Card>
        
        </>
    ))
    );
};

export default ProfileView;