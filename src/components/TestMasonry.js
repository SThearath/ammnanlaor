import React from 'react';
import Masonry from 'react-masonry-css';


const TestMasonry = props => {
    const myBreakpointsAndCols = {
        default: 3,
        960: 2,
        576: 1
      };

    let items = [
        {id: 1, name: 'My First Item'},
        {id: 2, name: 'Another item'},
        {id: 3, name: 'Third Item'},
        {id: 4, name: 'Here is the Fourth'},
        {id: 5, name: 'High Five'}
      ];
       
      // Convert array to JSX items
      items = items.map(function(item) {
        return <div key={item.id}>{item.name}</div>
      });

      return (
      <Masonry
        breakpointCols={myBreakpointsAndCols}
        className="my-masonry-grid"
        columnClassName="my-masonry-grid_column"
      >
        {items}
      </Masonry>
      );
};

export default TestMasonry;