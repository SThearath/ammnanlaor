import React from 'react';

const styles = {
    padding: "75px 10%",
    marginBottom: "50px"
};

const leftBorder = {
    marginTop: "10px",
    borderLeft: "5px solid #FFD505",
    paddingLeft: "20px"
};

const Banner = props => {

    return (
        <div style={styles} className="w-100 bg-gradient text-white">
            <h1>មិនដឹងថាអានសៀវភៅអ្វីមែនទេ?</h1>
            <h3 style={leftBorder}>តោះសាករកមើលគំនិតរបស់អ្នកដទៃទៅលើសៀវភៅ</h3>
        </div>
    );
};

export default Banner;