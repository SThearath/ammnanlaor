import React, { Fragment, useEffect, useState } from 'react';
import Axios from 'axios';
import Masonry from 'react-masonry-css';

import PostCard from './../posts/PostCard';
import PostCardNoImage from './../posts/PostCardNoImage';


const breakpoints = {
    default: 4,
    1100: 3,
    700: 2,
    500: 1
};

const PopularPosts = props => {

       
    return (
        <Fragment>
            <h3 className="mb-4 pb-3">ផុសពេញនិយម</h3>
            <Masonry breakpointCols={breakpoints}
                    className="my-masonry-grid"
                    columnClassName="my-masonry-grid_column">
            {
                props.posts.map(post => {
                    if (!post.imageUrl)
                        return (<PostCardNoImage key={post._id} post={post}/>);
                    return <PostCard key={post._id} post={post} redirect={props.redirect}/>
                })
            }
            </Masonry>
        </Fragment>
    );
};

export default PopularPosts;