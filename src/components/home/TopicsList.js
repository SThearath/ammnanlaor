import React, {Fragment} from 'react';
import {Link} from 'react-router-dom';
import {Row, Col, NavLink} from 'reactstrap';

const marginBottom = {
    marginBottom: "75px"
};

const color = ['red', 'yellow', 'blue', 'green'];

const TopicsList = props => {
    const {topics} = props;

    return (
        <Fragment>
            <h4 className="text-center">រកតាមរយៈប្រធានបទដែលអ្នកចូលចិត្ត</h4>
            <Row style={marginBottom} className="justify-content-center text-center mt-5">
            {
                topics.map((topic, index) => {
                    if (index === 0) {
                        return (
                            <Col xs={12} sm={8} md={6} xl={3} className="pr-xl-4 mb-2">
                                <NavLink to={`/by-topics/${topic._id}`} tag={Link} className={`py-3 card-${color[index%4]} text-white`}>{topic.name}</NavLink>
                            </Col>
                        );
                    } else if (index === (topics.size - 1)) {
                        return (
                            <Col xs={12} sm={8} md={6} xl={3} className="pl-xl-4 mb-2">
                                <NavLink to={`/by-topics/${topic._id}`} tag={Link} className={`py-3 card-${color[index%4]} text-white`}>{topic.name}</NavLink>
                            </Col>
                        )
                    }
                    return (
                        <Col xs={12} sm={8} md={6} xl={3} className="px-xl-4 mb-2">
                            <NavLink to={`/by-topics/${topic._id}`} tag={Link} className={`py-3 card-${color[index%4]} text-white`}>{topic.name}</NavLink>
                        </Col>
                    )
                })
            }
                {/*<Col xs={12} sm={8} md={6} xl={3} className="pr-xl-4 mb-2">*/}
                {/*    <NavLink to="/by-topics/5e348ce02e78a7eb189c4743" tag={Link} className={`py-3 card-${color[index%4} text-white`}>អក្សរសាស្ត្រ</NavLink>*/}
                {/*</Col>*/}
                {/*<Col xs={12} sm={8} md={6} xl={3} className="px-xl-4 mb-2">*/}
                {/*    <NavLink to="/by-topics/5e348cf12e78a7eb189c4744" tag={Link} className="py-3 card-yellow text-white">ប្រវត្តិសាស្ត្រ</NavLink>*/}
                {/*</Col>*/}
                {/*<Col xs={12} sm={8} md={6} xl={3} className="px-xl-4 mb-2">*/}
                {/*    <NavLink to="/by-topics/5e348d012e78a7eb189c4745" tag={Link} className="py-3 card-blue text-white">វិទ្យាសាស្ត្រ</NavLink>*/}
                {/*</Col>*/}
                {/*<Col xs={12} sm={8} md={6} xl={3} className="pl-xl-4 mb-2">*/}
                {/*    <NavLink to="/by-topics/5e348d122e78a7eb189c4746" tag={Link} className="py-3 card-green text-white">ការផ្សងព្រេង</NavLink>*/}
                {/*</Col>*/}
            </Row>
        </Fragment>
    );
};

export default TopicsList;