import React, {useEffect, useState} from 'react';
import Axios from "axios";

const styles = {
    padding: "50px 0",
    marginBottom: "50px"
};

const TopicBanner = props => {
    return (
        <div style={styles} className="w-100 bg-gradient text-white text-center">
            <h1>{props.topicName}</h1>
        </div>
    );
};

export default TopicBanner;