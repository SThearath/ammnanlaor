import React, {useEffect, useState} from 'react';
import Axios from 'axios';
import {NavLink} from 'reactstrap';
import {Link} from "react-router-dom";

import view from './../assets/viewer_icon.svg';


const QuestionCardView = (props) => {
    return (
        <div>
            {
                props.posts.map(post => (
                    <NavLink to={"/forum-details/" + post._id} tag={Link} className="card-white mb-1 ml-3 mr-3 text-dark p-0">
                        <div className="p-3">
                            <h4 className="mb-3 crop-text-2">{post.title}</h4>
                            {post.user && post.user.username} ~ 12/12/12
                            <label className="float-right">{post.views}</label>
                            <img className="iconView float-right" src={view} alt="ImageView"/>
                        </div>
                    </NavLink>
                ))
            }
        </div>
    )
};

export default QuestionCardView;