import Axios from 'axios';
import React, {useState} from 'react';
import {useForm} from 'react-hook-form';
import {Link, Redirect} from 'react-router-dom';
import {Button, Form, FormGroup, Label, Input} from 'reactstrap';
import Cookies from 'js-cookie';


const LoginForm = props => {
    const [redirect, setRedirect] = useState(false);
    const { handleSubmit, register, errors } = useForm();
    const onSubmit = values => {
        console.log(values)
        Axios.post('http://localhost:5000/auth/login', {
            email: values.email,
            password: values.password
        })
            .then(res => {
                Cookies.set('token', res.data.token, {expires: 1});
                Cookies.set('userId', res.data.userId, {expires: 1});
                console.log(res);
                setRedirect(true);
            })
            .catch(err => {
                console.log(err);
            });
    };

    if (redirect) {
        return <Redirect to="/"/>
    }
    return (
        <Form onSubmit={handleSubmit(onSubmit)}>
            <h3>ចូលគណនី</h3>
            <FormGroup className="mt-4">
                <Label>សារអេឡិចត្រូនិច</Label>
                <Input type="text"
                       className="mt-2"
                       placeholder="បញ្ចូល សារអេឡិចត្រូនិច"
                       name="email"
                       innerRef={register({
                           required: "សូមបញ្ចូលសារអេឡិចត្រូនិច",
                           pattern: {
                               value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i,
                               message: "មិនមែនជាសារអេឡិចត្រូនិច"
                           }
                       })}/>
                <span className="text-danger">{errors.email && errors.email.message}</span>
            </FormGroup>
            <FormGroup className="mt-4">
                <Label>លេខសំងាត់</Label>
                <Input type="password"
                       className="mt-2"
                       placeholder="បញ្ចូល លេខសំងាត់"
                       name="password"
                       innerRef={register({
                           required: "សូមបញ្ចូលលេខសំងាត់",
                           minLength: {
                               value: 8,
                               message: "សូមដាក់លេខសំងាត់អោយបាន ៨ខ្ទង់ឡើងទៅ"
                           }
                       })}/>
                <span className="text-danger">{errors.password && errors.password.message}</span>
            </FormGroup>

            <div className="form-group mt-4">
                <div className="custom-control custom-checkbox">
                    <input type="checkbox" className="custom-control-input" id="customCheck1" checked disabled/>
                    <label className="custom-control-label" htmlFor="customCheck1">ចងចាំ</label>
                </div>
            </div>

            <Button color="primary" type="submit" className="mt-5 btn-block">ចូល</Button>
            <p className="forgot-password text-right mb-1 mt-4">
                មិនទាន់មានគណនី ឬទេ? <Link to="/register">បង្កើតគណនីថ្មី?</Link>
            </p>
        </Form>
    );
}

export default LoginForm;