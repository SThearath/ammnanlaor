import React, {useState} from 'react';
import { Redirect } from "react-router-dom";
import {useForm} from 'react-hook-form';
import {Button, Form, FormGroup, Input, Label, CustomInput} from "reactstrap";
import Axios from "axios";
import Cookies from "js-cookie";
import TextEditor from "./TextEditor";


const CreatePostForm = props => {
    const [redirect, setRedirect] = useState(false);
    const [description, setDescription] = useState("");
    const { handleSubmit, register, errors, setValue } = useForm();
    const onSubmit = values => {
        console.log(values);
        let data = new FormData();
        data.set('title', values.title);
        data.set('content', description);
        data.set('topic', props.topic._id);
        data.append('image', values.file[0], values.file[0].filename)
        Axios.post('http://localhost:5000/api/posts', data, {
            headers: {'Authorization': `Bearer ${Cookies.get('token')}`}
        }).then(res => {
            console.log(res);
            setRedirect(true);
        }).catch(err => {
            console.log(err);
        })
    };

    let link = "/by-topics/" + props.match.params.topicId;
    if (redirect) {
        return <Redirect to={link}/>
    }
    return (
        <Form className="mb-2"  onSubmit={handleSubmit(onSubmit)} enctype="multipart/form-data">
            <FormGroup>
                <Label>ចំណងជើង *</Label>
                <Input type="text"
                       name="title"
                       innerRef={register({
                           required: "សូមបញ្ចូលចំណងជើង",
                       })}/>
                <span className="text-danger">{errors.title && errors.title.message}</span>
            </FormGroup>
            <FormGroup>
                <Label>អត្ថបទ</Label>
                <TextEditor setData={setDescription}/>
                {/*<Input style={{height: "350px"}}*/}
                {/*       type="textarea"*/}
                {/*       name="content"*/}
                {/*       innerRef={register({*/}
                {/*           required: "សូមបញ្ចូលអត្ថបទ"*/}
                {/*       })}/>*/}
                <span className="text-danger">{errors.content && errors.content.message}</span>
            </FormGroup>
            <FormGroup>
                <Label>រូបភាព *</Label>
                <CustomInput type="file"
                       name="file"
                       innerRef={register({})}/>
            </FormGroup>
            <div className="d-flex flex-end">
                <Button color="primary" className="ml-auto px-4" type="submit">បង្ហោះ</Button>
            </div>
        </Form>
    );
};

export default CreatePostForm;