import React, {Fragment, useState} from "react";
import {Input} from 'reactstrap';
import draftToHtml from 'draftjs-to-html';
import {Editor, EditorState, RichUtils, convertToRaw} from 'draft-js';

const PARAGRAPH_TYPES = [
    {label: 'Normal Text', style: 'div'},
    {label: 'Heading 1', style: 'header-one'},
    {label: 'Heading 2', style: 'header-two'},
    {label: 'Heading 3', style: 'header-three'},
    {label: 'Heading 4', style: 'header-four'},
    {label: 'Heading 5', style: 'header-five'},
    {label: 'Heading 6', style: 'header-six'}
];

const LIST_TYPES = [
    {label: 'OL', style: 'ordered-list-item'},
    {label: 'UL', style: 'unordered-list-item'}
];

const INLINE_STYLES = [
    {label: 'Bold', style: 'BOLD'},
    {label: 'Italic', style: 'ITALIC'},
    {label: 'Underline', style: 'UNDERLINE'}
];

const TextEditor = props => {
    const [editorState, setEditorState] = useState(EditorState.createEmpty());

    const onChange = editorState => {
        setEditorState(editorState);
        props.setData(draftToHtml(convertToRaw(editorState.getCurrentContent())));
    };

    const _toggleBlockType = blockType => {
        onChange(RichUtils.toggleBlockType(editorState, blockType));
    };

    const _toggleInlineStyle = inlineStyle => {
        onChange(RichUtils.toggleInlineStyle(editorState, inlineStyle));
    };

    const ParagraphTypeSelect = props => {
        const {editorState} = props;
        let selection = editorState.getSelection();
        let blockType = editorState
            .getCurrentContent()
            .getBlockForKey(selection.getStartKey())
            .getType();

        return (
            <Input type="select" className="mr-2"
                   style={{width: '180px'}}
                   defaultValue={blockType}
                   onChange={event => props.onSelect(event.target.value)} >
                {
                    PARAGRAPH_TYPES.map(type => (
                        <option key={type.label}
                                value={type.style}>
                            {type.label}
                        </option>
                    ))
                }
            </Input>
        )
    };

    const ListTypeButtons = props => {
        const {editorState} = props;
        let selection = editorState.getSelection();
        let blockType = editorState
            .getCurrentContent()
            .getBlockForKey(selection.getStartKey())
            .getType();

        let btnClasses = Array(2).fill('RichEditor-styleButton');
        btnClasses = btnClasses.map((btnClass, index) => {
            if (LIST_TYPES[index].style === blockType)
                btnClass += ' RichEditor-activeButton';
            return btnClass;
        });

        return (
            <div>
                <icon style={{cursor: 'pointer', padding: '7px'}}
                      className={`fas fa-lg fa-list-ol ${btnClasses[0]}`}
                      onMouseDown={event => {
                          event.preventDefault();
                          props.onClick(LIST_TYPES[0].style);
                      }}/>
                <icon style={{cursor: 'pointer', padding: '7px'}}
                      className={`fas fa-lg fa-list-ul ${btnClasses[1]}`}
                      onMouseDown={event => {
                          event.preventDefault();
                          props.onClick(LIST_TYPES[1].style);
                      }}/>
            </div>
        );
    };

    const InlineStyleButtons = props => {
        let currentStyle = props.editorState.getCurrentInlineStyle();

        let btnClasses = Array(3).fill('RichEditor-styleButton');
        btnClasses = btnClasses.map((btnClass, index) => {
            if (currentStyle.has(INLINE_STYLES[index].style))
                btnClass += ' RichEditor-activeButton';
            return btnClass;
        });

        return (
            <div>
                <icon style={{cursor: 'pointer', padding: '7px'}}
                      className={`fas fa-lg fa-bold ${btnClasses[0]}`}
                      onMouseDown={event => {
                          event.preventDefault();
                          props.onClick(INLINE_STYLES[0].style);
                      }}/>
                <icon style={{cursor: 'pointer', padding: '7px'}}
                      className={`fas fa-lg fa-italic ${btnClasses[1]}`}
                      onMouseDown={event => {
                          event.preventDefault();
                          props.onClick(INLINE_STYLES[1].style);
                      }}/>
                <icon style={{cursor: 'pointer', padding: '7px'}}
                      className={`fas fa-lg fa-underline ${btnClasses[2]}`}
                      onMouseDown={event => {
                          event.preventDefault();
                          props.onClick(INLINE_STYLES[2].style);
                      }}/>
            </div>
        );
    };

    // Render
    return (
        <Fragment>
            <div className="d-flex align-items-center justify-content-between"
                 style={{ paddingBottom: '5px', borderBottom: '1px solid #c8d4db' }}>
                <InlineStyleButtons editorState={editorState}
                                    onClick={_toggleInlineStyle}/>
                <ParagraphTypeSelect editorState={editorState}
                                     onSelect={_toggleBlockType}/>
                <ListTypeButtons editorState={editorState}
                                 onClick={_toggleBlockType}/>
            </div>
            <Editor style={{background: "#fff"}}
                    editorState={editorState}
                    onChange={editorState => onChange(editorState)}/>
        </Fragment>
    );
};

export default TextEditor;