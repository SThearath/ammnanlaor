import Axios from 'axios';
import React, {useState} from 'react';
import {useForm} from 'react-hook-form';
import {Link, Redirect} from 'react-router-dom';
import {Button, Form, FormGroup, Label, Input} from 'reactstrap';
import Cookies from "js-cookie";


const RegisterForm = props => {
    const [redirect, setRedirect] = useState(false);
    const { handleSubmit, register, errors } = useForm();
    const onSubmit = values => {
        console.log(values)
        Axios.post('http://localhost:5000/auth/register', {
            username: values.username,
            email: values.email,
            password: values.password,
            dob: values.dob
        })
            .then(res => {
                Cookies.set('token', res.token, {expires: 1});
                console.log(res)
                setRedirect(true);
            })
            .catch(err => {
                console.log(err)
            });
    };

    if (redirect) {
        return (
            <Redirect to="/"/>
        )
    }
    return (
        <Form onSubmit={handleSubmit(onSubmit)}>
            <h3>បង្កើតគណនី</h3>
            <FormGroup className="mt-5">
                <Label>ឈ្មោះ</Label>
                <Input type="text"
                       name="username"
                       placeholder="បញ្ចូល ឈ្មោះ"
                       innerRef={register({
                           required: "សូមបញ្ចូលឈ្មោះ"
                       })}/>
                <span className="text-danger">{errors.username && errors.username.message}</span>
            </FormGroup>
            <FormGroup className="mt-4">
                <Label>សារអេឡិចត្រូនិច</Label>
                <Input type="text"
                       placeholder="បញ្ចូល សារអេឡិចត្រូនិច"
                       name="email"
                       innerRef={register({
                           required: "សូមបញ្ចូលសារអេឡិចត្រូនិច",
                           pattern: {
                               value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i,
                               message: "មិនមែនជាសារអេឡិចត្រូនិច"
                           }
                       })}/>
                <span className="text-danger">{errors.email && errors.email.message}</span>
            </FormGroup>
            <FormGroup className="mt-4">
                <Label>លេខសំងាត់</Label>
                <Input type="password"
                       placeholder="បញ្ចូល លេខសំងាត់"
                       name="password"
                       innerRef={register({
                           required: "សូមបញ្ចូលលេខសំងាត់",
                           minLength: {
                               value: 8,
                               message: "សូមដាក់លេខសំងាត់អោយបាន ៨ខ្ទង់ឡើងទៅ"
                           }
                       })}/>
                <span className="text-danger">{errors.password && errors.password.message}</span>
            </FormGroup>
            <FormGroup className="mt-4">
                <Label>ថ្ងៃខែកំណើត</Label>
                <Input type="date"
                       name="dob"
                       innerRef={register({
                           required: "សូមរើសថ្ងៃ ខែ ឆ្នាំ កំណើត"
                       })}/>
                <span className="text-danger">{errors.dob && errors.dob.message}</span>
            </FormGroup>

            <Button color="secondary" type="submit" className="mt-5 btn-block">ចុះឈ្មាះ</Button>
            <p className="forgot-password text-right mb-1 mt-4">
                មានគណនីរួចហើយមែនទេ ? <Link to="/login">ចូលតាម គណនី?</Link>
            </p>
        </Form>
    );
}

export default RegisterForm;