import React from 'react';
import {Button, Col, Container, Row} from "reactstrap";

import {PostListView, TrendingListView} from './';
import TestMasonry from './TestMasonry';


const PostContent = () => {
    return (
        <Container fluid={true} className="mt-4">
            <Row>
                <Col xs="12" md="10" align="center">
                    <Button color="primary" className="rounded-button mb-4">បន្ថែមមតិ</Button>
                    {/* <PostListView/> */}
                    <TestMasonry />
                </Col>
                <Col xs="12" md="2"><TrendingListView/></Col>
            </Row>
        </Container>
    );
};

export default PostContent;