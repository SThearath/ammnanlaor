import React from 'react';
import Image from 'reactstrap'
import { Toast, ToastBody, ToastHeader } from 'reactstrap';


const filterList = [
    {
        id: '#:1',
        name: 'ព័ត៌មាន​ទូទៅ'
    },
    {
        id: '#:2',
        name: 'ផុសរបស់ខ្ញុំ'
    },
    {
        id: '#:3',
        name: 'ចម្លើយរបស់ខ្ញុំ'
    },
    {
        id: '#:4',
        name: 'ការកំណត់'
    },
    {
        id: '#:5',
        name: 'ចាកចេញ'
    },
];

const ProfileFeature = () => {
    return (
        <div className="rounded">
            <Toast>
                {filterList.map(item => (
                    <ToastBody className="toastbody">
                        <a href={item.id}>
                            {item.name}
                        </a>
                    </ToastBody>
                ))}
            </Toast>
        </div>
    )
};

export default ProfileFeature;