import React, {useState} from 'react';

import PostCardView from './PostCardView';
import {POSTS} from './../constants'


const PostListView = () => {

    const [posts] = useState(POSTS);

    return (
        <div className="text-left mx-1 mx-sm-3 mx-lg-3 mx-xl-5">
            {
                posts.map((post) => (
                    <PostCardView key={post.id} post={post} />
                ))
            }
        </div>
    );
};

export default PostListView;