import React from 'react';
import {Link} from 'react-router-dom';


const Trending = (props) => {
    const trendingPosts = props.trendingPosts;

    return (
        <div className="mb-3 pb-2 ml-md-3">
            <h5 className="trending-header">បច្ចេកវិទ្យា</h5>
            <div className="d-flex flex-column">
                {
                    trendingPosts.map((trendingPost) => (
                        <Link key={trendingPost.id} className="trending-link" to={trendingPost.link}>
                            <i className="fas fa-angle-right mr-2"/>{trendingPost.title}
                        </Link>
                    ))
                }
            </div>
        </div>
    );
};

export default Trending;