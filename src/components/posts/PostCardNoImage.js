import React from 'react';
import {NavLink} from 'reactstrap';
import {Link} from 'react-router-dom';
import ReactHtmlParser from 'react-html-parser';


const PostCardNoImage = props => {
    let link = "/post-detail/" + props.post._id;
    return (
        <NavLink to={link} tag={Link} className="card-white mb-4 text-dark p-3">
            <h4 className="mb-2 crop-text-2">{props.post.name}</h4>
            <p className="crop-text-4">{ReactHtmlParser(props.post.content)}</p>
            <p className="mb-0">លឹម គឹមស្រេង  ~ ១ មករា ២០២០ <span className="btn btn-sm">ប្រវត្តិសាស្ត្រ</span></p>
        </NavLink>
    );
};

export default PostCardNoImage;
