import React, {Fragment} from "react";
import {Button, Col, Row, Form, InputGroup, Input, InputGroupAddon} from 'reactstrap';

import CommentsList from "./CommentsList";
import {useForm} from "react-hook-form";
import Axios from "axios";
import Cookies from "js-cookie";


const Comments = props => {
    const { handleSubmit, register, errors, setValue } = useForm();
    const onSubmit = values => {
        Axios.post(`http://localhost:5000/api/posts/${props.post._id}/comments`, {
            "user": Cookies.get('userId'),
            "comment": values.comment
        }).then(res => {
            setValue('comment', '');
            props.syncComments();
            console.log(res)
        }).catch(err => {
            console.log(err)
        })
    };

    return (
        <Fragment>
            <h3>បញ្ចេញមតិ</h3>
            <Row className="justify-content-center mt-4 mb-5">
                <Col md={8}>
                    <Form onSubmit={handleSubmit(onSubmit)}>
                        <InputGroup>
                            <Input type="text"
                                   name="comment"
                                   innerRef={register({})}/>
                            <InputGroupAddon addonType="append">
                                <Button type="submit" color="primary">ផ្តល់ចម្លើយ</Button>
                            </InputGroupAddon>
                        </InputGroup>
                    </Form>
                </Col>
            </Row>
            <h3>មតិ</h3>
            <Row className="justify-content-center mt-4">
                <Col md={8}>
                    <CommentsList comments={props.comments}/>
                </Col>
            </Row>
        </Fragment>
    );
};

export default Comments