import React, {Fragment} from 'react';
import ReactHtmlParser from 'react-html-parser';
import Comments from "./Comments";


const PostDetailContent = props => {
    let image = 'http://localhost:5000' + props.post.imageUrl;
    let date = new Date(props.post.createdAt);

    const getDate = () => (
        `${date.getDay()}/${date.getMonth()}/${date.getFullYear()} ម៉ោង ${date.getHours()}:${date.getMinutes()} នាទី`
    );

    return (
        <Fragment>
            <h3 className="mb-2 pb-2" >{props.post.title}</h3>
            <p>បង្ហោះដោយ <span className="text-primary">{props.post.user && props.post.user.username}</span> <span className="text-faded pb-4">នៅ { getDate() }</span></p>
            <div className="mt-4 d-flex justify-content-center">
                <img className="mb-4 img-detail" src={image} />
            </div>
            <div className="mb-5 mt5">{ ReactHtmlParser(props.post.content) }</div>
            <hr/>
            <Comments post={props.post} comments={props.comments} syncComments={props.syncComments}/>
        </Fragment>
    )
};

export default PostDetailContent;