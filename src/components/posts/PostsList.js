import React, {Fragment, useEffect, useState} from 'react';
import Masonry from 'react-masonry-css';

import PostCard from './../posts/PostCard';
import PostCardNoImage from './../posts/PostCardNoImage';
import PopularPosts from "../home/PopularPosts";


const PostsList = props => {
    const myBreakpointsAndCols = {
        default: 3,
        960: 2,
        576: 1
    };

       
    return (
        <Fragment>
            <Masonry breakpointCols={myBreakpointsAndCols}
                    className="my-masonry-grid"
                    columnClassName="my-masonry-grid_column">
            {
                props.posts.map(post => {
                    if (!post.imageUrl)
                        return (<PostCardNoImage key={post._id} post={post}/>);
                    return <PostCard key={post._id} post={post} redirect={props.redirect}/>
                })
            }
            </Masonry>
        </Fragment>
    );
};

export default PostsList;