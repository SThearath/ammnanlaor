import React from "react";
import {ListGroup, ListGroupItem, ListGroupItemText} from 'reactstrap';


const CommentsList = props => {


    return (
        <ListGroup flush>
            {
                props.comments.map(comment => {
                    let date = new Date(comment.createdAt);

                    const getDate = () => (
                        `${date.getDay()}/${date.getMonth()}/${date.getFullYear()} ម៉ោង ${date.getHours()}:${date.getMinutes()} នាទី`
                    );

                    return (
                        <ListGroupItem className="bg-transparent">
                            <h6 className="mb-2">{comment.user && comment.user.username} <span
                                className="text-faded">{getDate()}</span></h6>
                            <span>{comment.comment}</span>
                        </ListGroupItem>
                    )
                })
            }
        </ListGroup>
    );
};

export default CommentsList;