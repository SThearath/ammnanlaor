import React from 'react';
import {NavLink} from 'reactstrap';
import {Link, Redirect} from 'react-router-dom';
import ReactHtmlParser from 'react-html-parser';
import Axios from "axios";
import Cookies from "js-cookie";


const PostCard = props => {
    let link = "/post-detail/" + props.post._id;
    let image = 'http://localhost:5000' + props.post.imageUrl;
    let date = new Date(props.post.createdAt);

    const getDate = () => (
        `${date.getDay()}/${date.getMonth()}/${date.getFullYear()} ម៉ោង ${date.getHours()}:${date.getMinutes()} នាទី`
    );

    const handleClick = event => {
        event.preventDefault();
        props.redirect(link);
        console.log(props.post)
        console.log('hello world')
        Axios
            .patch(`http://localhost:5000/api/posts/${props.post._id}`, {
                views: props.post.views + 1
            })
            .then(res => {
                console.log(res);
            })
            .catch(err => {
                console.log(err);
            })
    };

    const handleUpvote = event => {
        event.preventDefault();
        console.log("Upvote")
        Axios.post(`http://localhost:5000/api/posts/${props.post._id}/upvote`, {}, {
            headers: {'Authorization': `Bearer ${Cookies.get('token')}`}
        }).then(res => {
            console.log(res);
        }).catch(err => {
            console.log(err);
        })
    }

    const handleDownvote = event => {
        event.preventDefault();
        Axios.post(`http://localhost:5000/api/posts/${props.post._id}/downvote`, {}, {
            headers: {'Authorization': `Bearer ${Cookies.get('token')}`}
        }).then(res => {
            console.log(res)
        }).catch(err => {
            console.log(err)
        })
    }

    let style =[];
    const {upvotes} = props.post;
    upvotes.map(upvote => {
        if (Cookies.get('userId') === upvote.user) {
            style[0] = {color: '#1CACF4'};
        }
    })
    const {downvotes} = props.post;
    downvotes.map(downvote => {
        if (Cookies.get('userId') === downvote.user) {
            style[1] = {color: '#1CACF4'};
        }
    })


    return (
        <div style={{zIndex: -2}} className="card-white mb-4 text-dark p-0">
            <div className="card-img-wrapper" onMouseDown={handleClick}>
                <img className="w-100" src={image} alt="here"/>
            </div>
            <div className="p-3">
                <h4 className="mb-2 crop-text-2">{props.post.title}</h4>
                <p className="crop-text-2">{props.post.content.replace(/<[^>]+>/g, '')}</p>
                <p className="mb-2">{props.post.user.username} <span className="text-faded">នៅ {getDate()}</span></p>
                <span>
                    <span className="badge badge-primary mr-3">{props.post.topic.name}</span>
                    <icon className="fas fa-eye"/>
                    {props.post.views}
                    <icon className="fas fa-thumbs-up ml-3" style={style[0]} onMouseDown={handleUpvote}/>
                    {'' + props.post.upvotes.length}
                    <icon className="fas fa-thumbs-down ml-2"  style={style[1]} onMouseDown={handleDownvote}/>
                    {'' + props.post.downvotes.length}
                    <icon className="fas fa-comment-alt ml-3"/>
                </span>
            </div>
        </div>
    );
};

export default PostCard;
