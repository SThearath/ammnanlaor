import React,{useState,useEffect} from 'react';
import { Toast, ToastBody, ToastHeader, NavLink } from 'reactstrap';
import axios from 'axios';

const Filter = (props) => {
    const [posts, setState] = useState({posts: []});
    useEffect(()=>{
        axios.get('http://localhost:5000/api/topics')
        .then(res => {
            setState({posts: res.data})
            console.log(res)
        })
        .catch(err => console.log(err));
    }, []);

    return(
        <div className="rounded​">
        <Toast> 
            <ToastHeader className="toastheader p-4 text-light text-center rounded bg-primary">ជម្រើស</ToastHeader>
          {posts.posts.map(item => (
          <ToastBody className="toastbody">
            <NavLink href={item.id}>
                {item.name}
            </NavLink>
          </ToastBody>
          ))}
        </Toast>
      </div>
    )
};

export default Filter;