import React, {useEffect, useState} from 'react';
import {Link} from 'react-router-dom';
import Axios from "axios";


const ChangeTopics = props => {
    const [topics, setTopics] = useState([]);

    useEffect(() => {
        syncTopics();
    }, []);


    const syncTopics = () => {
        Axios.get('http://localhost:5000/api/topics')
            .then(res => {
                setTopics(res.data);
            })
            .catch(err => {
                console.log(err);
            })
    }

    return (
        <div className="mb-3 pb-2">
            <h4 className=" mb-3 ml-md-3"><icon className="fas fa-atom mr-2"/>ប្រធានបទដទៃទៀត</h4>
            <div className="d-flex flex-column ml-md-3">
            {
                topics && topics.map(topic => {
                    return (
                        <Link key={topic._id} className="trending-link mt-2" to={`/by-topics/${topic._id}`}>
                            <h6>{topic.name}</h6>
                        </Link>
                    );
                })
            }
            {/*<div className="mt-2">*/}
            {/*    <Label>ក្រៅពីនេះ</Label>*/}
            {/*    <Input type="select">*/}
            {/*        <option>ជ្រើសរើសប្រធានបទ</option>*/}
            {/*        <option>2</option>*/}
            {/*        <option>3</option>*/}
            {/*        <option>4</option>*/}
            {/*        <option>5</option>*/}
            {/*    </Input>*/}
            {/*</div>*/}
            </div>
        </div>
    );
};

export default ChangeTopics;