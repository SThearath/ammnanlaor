const express = require('express')
const mongoose = require('mongoose')
const bodyParser = require('body-parser')


// Initialize
const app = express()

app.use(bodyParser.urlencoded({extended: true}))
app.use(bodyParser.json())

const port = process.env.PORT || 5000


// Connect to MongoDB
const connectionString = 'mongodb+srv://admin:admin@amnandatabase-molts.mongodb.net/test?retryWrites=true&w=majority'
mongoose.connect(connectionString, {useNewUrlParser: true, useUnifiedTopology: true})
mongoose.connection.on('error', err => {
    console.log('err: ', err)
})

mongoose.connection.on('connected', (err, res) => {
    console.log('MongoDB is connected')
})


// Config Swagger
const swaggerJsDoc = require('swagger-jsdoc')
const swaggerUi = require('swagger-ui-express')

const swaggerOptions = {
    swaggerDefinition: {
        info: {
            title: 'Amnan Laor API',
            description: 'API Information for AmnanLaor website',
            contact: {
                name: 'Someone'
            },
            servers: [`http://localhost:${port}`]
        }
    },
    apis: ['./api/routes/auth/index.js', './api/routes/api/users.js', './api/routes/api/posts.js', './api/routes/api/question.js', './api/routes/api/topics.js']
}

const swaggerDocs = swaggerJsDoc(swaggerOptions)
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocs))

const cors = require('cors')
app.use(cors());

// Connect Routes
const routes = require('./routes')
app.use('/', routes)

// Make image public
app.use('/uploads', express.static('./api/uploads'))

app.listen(port, () => console.log(`Api app listening on port ${port}!`))