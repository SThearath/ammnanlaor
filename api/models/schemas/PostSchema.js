const Schema = require('mongoose').Schema

const VoteSchema = require('./VoteSchema')


const CommentSchema = new Schema({
    comment: String,
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    }
}, {
    timestamps: true
});

const PostSchema = new Schema({
    title: String,
    content: String,
    imageUrl: String,
    views: Number,
    upvotes: [VoteSchema],
    downvotes: [VoteSchema],
    comments: [CommentSchema],
    topic: {
        type: Schema.Types.ObjectId,
        ref: 'Topic'
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    }
}, {
    timestamps: true
})

module.exports = {CommentSchema, PostSchema}