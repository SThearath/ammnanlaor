const Schema = require('mongoose').Schema


const VoteSchema = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    }
})

module.exports = VoteSchema