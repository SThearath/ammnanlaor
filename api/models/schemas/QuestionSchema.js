const Schema = require('mongoose').Schema

const AnswerSchema = new Schema({
    answer: String,
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    }
})

const QuestionSchema = new Schema({
    title: String,
    views: Number,
    topic: {
        type: Schema.Types.ObjectId,
        ref: 'Topic'
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    answers: [AnswerSchema]
}, {
    timestamps: true
})

module.exports = {AnswerSchema, QuestionSchema}