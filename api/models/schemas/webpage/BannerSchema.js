const Schema = require('mongoose').Schema


const BannerSchema = new Schema({
    imageUrl: String,
    publicationStatus: Boolean,
    topic: {
        type: Schema.Types.ObjectId,
        ref: 'Topic'
    },
    uploadedBy: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    }
}, {
    timestamps: true
})

module.exports = BannerSchema