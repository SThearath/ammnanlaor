const Schema = require('mongoose').Schema


const UserSchema = new Schema({
    username: String,
    email: String,
    password: String,
    dob: Date,
    role: {
        type: Schema.Types.ObjectId,
        ref: 'Role'
    }
})

module.exports = UserSchema