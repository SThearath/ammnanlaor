const mongoose = require('mongoose')

module.exports = {
    Answer: mongoose.model('Answer', require('./schemas/QuestionSchema').AnswerSchema),
    Vote: mongoose.model('Vote', require('./schemas/VoteSchema')),
    Comment: mongoose.model('Comment', require('./schemas/PostSchema').CommentSchema),
    Post: mongoose.model('Post', require('./schemas/PostSchema').PostSchema),
    Question: mongoose.model('Question', require('./schemas/QuestionSchema').QuestionSchema),
    Topic: mongoose.model('Topic', require('./schemas/TopicSchema')),
    User: mongoose.model('User', require('./schemas/UserSchema'))
}