const {User} = require('../models')


exports.all = async (req, res) => {
    await User.find()
        .exec()
        .then(users => {
            res.status(200).json(users)
        })
        .catch(error => {
            res.status(500).json({error})
        })
}

exports.findById = async (req, res) => {
    await User.findById(req.params.userId)
        .exec()
        .then(user => {
            if (user == null) {
                res.status(404).json({message: "User not found"})
            }
            res.status(200).json(user)
        })
        .catch(error => {
            res.status(500).json({error})
        })
}

exports.create = async (req, res) => {
    let {username, email, password, dob} = req.body
    let user = new User({username, email, password, dob})
    await user.save()
        .then(user => {
            res.status(200).json(user)
        })
        .catch(error => {
            res.status(500).json({error})
        })
}

exports.update = async (req, res) => {
    await User.findById(req.params.userId)
        .exec()
        .then(user => {
            if (user == null) {
                res.status(404).json({message: "User not found"})
            }
            let {username, email, password, dob} = req.body
            let updatedUser = {}
            if (username) { updatedUser.username = username }
            if (email) { updatedUser.email = email }
            if (password) { updatedUser.password = password }
            if (dob) { updatedUser.dob = dob }
            User.update({_id: req.params.userId}, {$set: updatedUser})
                .exec()
                .then(result => {
                    res.status(200).json(user)
                })
                .catch(error => {
                    res.status(500).json({error})
                })
        })
        .catch(error => {
            res.status(500).json({error})
        })
}

exports.delete = async (req, res) => {
    await User.findById(req.params.userId)
        .exec()
        .then(user => {
            if (user == null) {
                res.status(404).json({message: "User not found"})
            }
            User.remove({_id: req.params.userId})
                .then(result => {
                    res.status(200).json(user)
                })
                .catch(error => {
                    res.status(500).json({error})
                })
        })
        .catch(error => {
            res.status(500).json({error})
        })
}