const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')

const {User} = require('../models')


exports.register = async (req, res) => {
    await bcrypt.hash(req.body.password, 10, (err, hash) => {
        if (err) {
            return res.status(500).json({err})
        }
        let user = new User({
            username: req.body.username,
            email: req.body.email,
            password: hash,
            dob: req.body.dob
        })
        user.save()
            .then(user => {
                let token = jwt.sign(
                    {userId: user._id},
                    'secret',
                    {expiresIn: "24h"}
                );
                res.status(200).json({user, token})
            })
            .catch(error => {
                res.status(500).json({error})
            })
    })
}

exports.login = async (req, res) => {
    await User.find({email: req.body.email})
        .exec()
        .then(user => {
            if (user.length < 1) {
                return res.status(401).json({message: 'Auth failed'})
            }
            bcrypt.compare(req.body.password, user[0].password, (err, result) => {
                if (err) {
                    return res.status(500).json({message: 'Auth failed'})
                }
                if (result) {
                    let token = jwt.sign(
                        {userId: user[0]._id},
                        'secret',
                        {expiresIn: "10h"}
                    );
                    return res.status(200).json({message: 'Auth successful', token, userId: user[0]._id})
                }
                return res.status(500).json({message: 'Auth failed'})
            })
        })
}