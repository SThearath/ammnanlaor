const {Post, Comment, Vote} = require('../models')


exports.all = async (req, res) => {
    await Post.find()
        .populate('user')
        .exec()
        .then(posts => {
            res.status(200).json(posts)
        })
        .catch(error => {
            res.status(500).json({error})
        })
}

exports.findById = async (req, res) => {
    console.log(req.params.postId)
    await Post.findById(req.params.postId)
        .populate('user topic')
        .exec()
        .then(post => {
            if (post == null) {
                res.status(404).json({message: "Post not found"})
            }
            res.status(200).json(post)
        })
        .catch(error => {
            res.status(500).json({error})
        })
}

exports.findByTopic = async (req, res) => {
    await Post.find({topic: req.params.topicId})
        .populate('user topic')
        .exec()
        .then(posts => {
            res.status(200).json(posts)
        })
        .catch(error => {
            res.status(500).json({error})
        })
}

exports.create = async (req, res) => {
    let {title, content, topic} = req.body
    let post = new Post({ title, content, topic,
        imageUrl: '/uploads/' + req.file.filename,
        views: 0, upvotes: [], downvotes: [],
        user: req.userId
    })
    await post.save()
        .then(post => {
            res.status(200).json(post)
        })
        .catch(error => {
            res.status(500).json({error})
        })
}

exports.update = async (req, res) => {
    let {topic, title, content, imageUrl, views, upvotes, downvotes} = req.body
    let updatedPost = {}
    if (topic) { updatedPost.topic = topic; }
    if (title) { updatedPost.user = title; }
    if (content) { updatedPost.user = content; }
    if (imageUrl) { updatedPost.imageUrl = imageUrl; }
    if (views) { updatedPost.views = views; }
    if (upvotes) { updatedPost.upvotes = upvotes; }
    if (downvotes) { updatedPost.downvotes = downvotes; }
    await Post.findById(req.params.postId)
        .exec()
        .then(post => {
            if (post == null) {
                res.status(404).json({message: "Post not found"})
            }
            Post.update({_id: req.params.postId}, {$set: updatedPost})
                .exec()
                .then(result => {
                    res.status(200).json(post)
                })
                .catch(error => {
                    res.status(500).json({error})
                })
        })
        .catch(error => {
            res.status(500).json({error})
        })
}

exports.delete = async (req, res) => {
    await Post.findById(req.params.postId)
        .exec()
        .then(post => {
            if (post == null) {
                res.status(404).json({message: "Post not found"})
            }
            Post.remove({_id: req.params.postId})
                .then(result => {
                    res.status(200).json(post)
                })
                .catch(error => {
                    res.status(500).json({error})
                })
        })
        .catch(error => {
            res.status(500).json({error})
        })
}

exports.upvote = async (req, res) => {
    console.log("upvote")
    await Post.findById(req.params.postId)
        .exec()
        .then(post => {
            let vote = new Vote({
                user: req.userId
            })
            Post.update({_id: req.params.postId}, { $push: { upvotes: vote }})
                .exec()
                .then(result => {
                    res.status(200).json(vote)
                })
                .catch(error => {
                    res.status(500).json({error})
                })
        })
        .catch(error => {
            res.status(500).json({error})
        })
}

exports.downvote = async (req, res) => {
    await Post.findById(req.params.postId)
        .exec()
        .then(post => {
            let vote = new Vote({
                user: req.userId
            })
            Post.update({_id: req.params.postId}, { $push: { downvotes: vote }})
                .exec()
                .then(result => {
                    res.status(200).json(vote)
                })
                .catch(error => {
                    res.status(500).json({error})
                })
        })
        .catch(error => {
            res.status(500).json({error})
        })
}

exports.getComments = async (req, res) => {
    await Post.findById(req.params.postId)
        .populate('comments.user')
        .exec()
        .then(post => {
            res.status(200).json(post.comments)
        })
        .catch(error => {
            res.status(500).json({error})
        })
}

exports.createComment = async (req, res) => {
    await Post.findById(req.params.postId)
        .exec()
        .then(post => {
            let comment = new Comment({
                comment: req.body.comment,
                user: req.body.user
            })
            Post.update({_id: req.params.postId}, { $push: { comments: comment }})
                .exec()
                .then(result => {
                    res.status(200).json(comment)
                })
                .catch(error => {
                    res.status(500).json({error})
                })
        })
        .catch(error => {
            res.status(500).json({error})
        })
}