const jwt = require('jsonwebtoken')

module.exports = (req, res, next) => {
    try {
        console.log(req.headers)
        let decoded = jwt.verify(req.headers.authorization.split(" ")[1], 'secret');
        req.userId = decoded.userId
        console.log(decoded.userId)
        next()
    } catch (err) {
        res.status(401).json({message: 'Unauthenticated'})
    }
}