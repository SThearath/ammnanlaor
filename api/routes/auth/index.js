const authRoutes = require('express').Router()

const AuthController = require('../../controllers/AuthController')


/**
 * @swagger
 *
 * tags:
 *   - name: Auth
 *     description: Api authentication
 *
 */


/**
 * @swagger
 *
 * /auth/register:
 *   post:
 *     description: Creates an account
 *     tags:
 *       - Auth
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: user
 *         description: User object
 *         in:  body
 *         required: true
 *         type: object
 *         schema:
 *           $ref: '#/definitions/User'
 *     responses:
 *       200:
 *         description: new topic
 *         schema:
 *           $ref: '#/definitions/User'
 *       500:
 *          description: Internal server Error
 */
authRoutes.post('/register', AuthController.register)


/**
 * @swagger
 *
 * /auth/login:
 *   post:
 *     description: Logs into an account
 *     tags:
 *       - Auth
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: user
 *         description: User object
 *         in:  body
 *         required: true
 *         type: object
 *         properties:
 *           email:
 *              type: string
 *           password:
 *              type: string
 *     responses:
 *       200:
 *         description: token
 *         properties:
 *            message:
 *                type: string
 *            token:
 *                type: string
 *
 *       500:
 *          description: Internal server Error
 */
authRoutes.post('/login', AuthController.login)

module.exports = authRoutes