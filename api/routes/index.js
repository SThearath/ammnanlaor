const routes = require('express').Router()


// Api routes
const apiRoutes = require('./api')
routes.use('/api', apiRoutes)

// Auth routes
const authRoutes = require('./auth')
routes.use('/auth', authRoutes)

// Other routes
routes.get('/', (req, res) => res.send('Hello World!'))

module.exports = routes