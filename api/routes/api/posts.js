const router = require('express').Router()

const authenticate = require('../../middleware/authenticate')
const upload = require('../../middleware/upload')
const PostController = require('../../controllers/PostController')


/**
 * @swagger
 *
 * tags:
 *   - name: Posts
 *     description: blog posts
 *
 * definitions:
 *   Post:
 *     type: object
 *     required:
 *       - heading
 *       - content
 *       - user
 *       - topic
 *       - imageUrl
 *     properties:
 *       heading:
 *         type: string
 *       content:
 *         type: string
 *       user:
 *         type: string
 *       topic:
 *         type: string
 *       imageUrl:
 *         type: string
 *   Posts:
 *          type: array
 *          items:
 *              $ref: '#/definitions/Post'
 */


/**
 * @swagger
 *
 * /api/posts:
 *   get:
 *     description: Get a list of posts
 *     tags:
 *      - Posts
 *     responses:
 *       200:
 *          description: Success
 *          schema:
 *              $ref: '#/definitions/Posts'
 *       500:
 *          description: Internal Server Error
 */
router.get('/', PostController.all)

router.get('/:topicId', PostController.findByTopic)

router.get('/fetch', (req, res) => {
    console.log(req.query.title)
    // Post.find({title: {$regex: req.query.title, $options: 1}})
    //     .populate('user topic')
    //     .exec()
    //     .then(posts => {
    //         res.status(200).json(posts)
    //     })
    //     .catch(error => {
    //         res.status(500).json({error})
    //     })
})

/**
 * @swagger
 *
 * /api/posts:
 *   post:
 *     description: Creates a Post
 *     tags:
 *       - Posts
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: header
 *         name: Authorization
 *         schema:
 *              type: string
 *              required: true
 *       - name: post
 *         description: Post object
 *         in:  body
 *         required: true
 *         type: string
 *         schema:
 *           $ref: '#/definitions/Post'
 *     responses:
 *       200:
 *         description: users
 *         schema:
 *           $ref: '#/definitions/Post'
 */
router.post('/',
    [authenticate, upload],
    PostController.create
)


/**
 * @swagger
 *
 * /api/posts/details/{postId}:
 *   get:
 *     description: Get a post by id
 *     tags:
 *      - Posts
 *     parameters:
 *      - in: path
 *        name: postId
 *        schema:
 *              type: integer
 *        required: true
 *        description: Numeric ID of the post to get
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Success
 *         schema:
 *             $ref: '#/definitions/Post'
 */
router.get('/details/:postId', PostController.findById)


/**
 * @swagger
 *
 * /api/posts/{postId}:
 *   patch:
 *     description: Updates a Post by id
 *     tags:
 *       - Posts
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: path
 *         name: postId
 *         schema:
 *              type: integer
 *         required: true
 *         description: Numeric ID of the post to update
 *       - name: post
 *         description: Post object
 *         in:  body
 *         required: true
 *         type: string
 *         schema:
 *           $ref: '#/definitions/Post'
 *     responses:
 *       200:
 *         description: users
 *         schema:
 *           $ref: '#/definitions/Post'
 */
router.patch('/:postId', PostController.update)

/**
 * @swagger
 *
 * /api/posts/{postId}:
 *   delete:
 *     description: Delete a Post by id
 *     tags:
 *       - Posts
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: path
 *         name: postId
 *         schema:
 *              type: integer
 *         required: true
 *         description: Numeric ID of the post to delete
 *     responses:
 *       200:
 *         description: users
 *         schema:
 *           $ref: '#/definitions/Post'
 */
router.delete('/:postId', [authenticate], PostController.delete)

router.post('/:postId/upvote',  [authenticate], PostController.upvote)

router.post('/:postId/downvote',  [authenticate], PostController.downvote)

router.get('/:postId/comments', PostController.getComments)

router.post('/:postId/comments', PostController.createComment)

module.exports = router;