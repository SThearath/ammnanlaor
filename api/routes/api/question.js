const router = require('express').Router()

const {Answer, Question} = require('../../models')


/**
 * @swagger
 *
 * tags:
 *   - name: Questions
 *     description: questions in forum
 *
 * definitions:
 *
 *   Answer:
 *     type: object
 *     required:
 *       - _id
 *       - answer
 *       - user
 *     properties:
 *       _id:
 *         type: string
 *       answer:
 *         type: string
 *       user:
 *         type: string
 *
 *   Answers:
 *          type: array
 *          items:
 *              $ref: '#/definitions/Answer'
 *
 *   Question:
 *     type: object
 *     required:
 *       - _id
 *       - title
 *       - views
 *       - topic
 *       - user
 *     properties:
 *       _id:
 *         type: string
 *       title:
 *         type: string
 *       views:
 *         type: integer
 *       topic:
 *         type: string
 *       user:
 *         type: string
 *   Questions:
 *          type: array
 *          items:
 *              $ref: '#/definitions/Question'
 */


/**
 * @swagger
 *
 * /api/questions:
 *   get:
 *     description: Get a list of questions
 *     tags:
 *      - Questions
 *     responses:
 *       200:
 *          description: questions
 *          schema:
 *              $ref: '#/definitions/Questions'
 *       500:
 *          description: Internal Server Error
 */
router.get('/', (req, res) => {
    Question.find()
        .populate('user topic answers.user')
        .exec()
        .then(questions => {
            res.status(200).json(questions)
        })
        .catch(error => {
            res.status(500).json({error})
        })
})


/**
 * @swagger
 *
 * /api/questions:
 *   post:
 *     description: Creates a Question
 *     tags:
 *       - Questions
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: question
 *         description: Question object
 *         in:  body
 *         required: true
 *         type: object
 *         schema:
 *           $ref: '#/definitions/Question'
 *     responses:
 *       200:
 *         description: new question
 *         schema:
 *           $ref: '#/definitions/Question'
 *       500:
 *          description: Internal server Error
 */
router.post('/', (req, res) => {
    let question = new Question({
        title: req.body.title,
        views: 0,
        topic: req.body.topic,
        user: req.body.user
    })
    question.save()
        .then(question => {
            res.status(200).json(question)
        })
        .catch(error => {
            res.status(500).json({error})
        })
})


/**
 * @swagger
 *
 * /api/questions/{questionId}:
 *   get:
 *     description: Get a question by id
 *     tags:
 *      - Questions
 *     parameters:
 *      - in: path
 *        name: questionId
 *        schema:
 *              type: string
 *        required: true
 *        description: ID of the question to get
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: question
 *         schema:
 *             $ref: '#/definitions/Question'
 *       404:
 *          description: question not found
 *       500:
 *          description: Internal server Error
 */
router.get('/:questionId', (req, res) => {
    Question.findById(req.params.questionId)
        .exec()
        .then(question => {
            if (question == null) {
                res.status(404).json({message: "User not found"})
            }
            res.status(200).json(question)
        })
        .catch(error => {
            res.status(500).json({error})
        })
})


/**
 * @swagger
 *
 * /api/questions/{questionId}:
 *   patch:
 *     description: Updates a Question by id
 *     tags:
 *       - Questions
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: path
 *         name: questionId
 *         schema:
 *              type: string
 *         required: true
 *         description: ID of the question to update
 *       - name: question
 *         description: Question object
 *         in:  body
 *         required: true
 *         type: object
 *         schema:
 *           $ref: '#/definitions/Question'
 *     responses:
 *       200:
 *         description: updated question
 *         schema:
 *           $ref: '#/definitions/Question'
 *       404:
 *          description: question not found
 *       500:
 *          description: Internal server Error
 */
router.patch('/:questionId', (req, res) => {
    let id = req.params.questionId
    Question.findById(id)
        .exec()
        .then(question => {
            if (question == null) {
                res.status(404).json({message: "Question not found"})
            }
            let updatedQuestion = {}
            if (req.body.title) {
                updatedQuestion.title = req.body.title
                question.title = req.body.title
            }
            if (req.body.topic) {
                updatedQuestion.topic = req.body.topic
                question.topic = req.body.topic
            }
            if (req.body.views) {
                updatedQuestion.views = req.body.views
                question.views = req.body.views
            }

            Question.update({_id: id}, {$set: updatedQuestion})
                .exec()
                .then(result => {
                    res.status(200).json(question)
                })
                .catch(error => {
                    res.status(500).json({error})
                })
        })
        .catch(error => {
            res.status(500).json({error})
        })
})


/**
 * @swagger
 *
 * /api/questions/{questionId}:
 *   delete:
 *     description: Delete a Question by id
 *     tags:
 *       - Questions
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: path
 *         name: questionId
 *         schema:
 *              type: string
 *         required: true
 *         description: ID of the question to delete
 *     responses:
 *       200:
 *         description: deleted question
 *         schema:
 *           $ref: '#/definitions/Question'
 *       404:
 *          description: question not found
 *       500:
 *         description: Internal server Error
 */
router.delete('/:questionId', (req, res) => {
    let id = req.params.questionId
    Question.findById(id)
        .exec()
        .then(question => {
            if (question == null) {
                res.status(404).json({message: "Question not found"})
            }
            Question.remove({_id: id})
                .then(result => {
                    res.status(200).json(question)
                })
                .catch(error => {
                    res.status(500).json({error})
                })
        })
        .catch(error => {
            res.status(500).json({error})
        })
})


/**
 * @swagger
 *
 * /api/questions/{questionId}/answers:
 *   get:
 *     description: Get a answers of a question by id
 *     tags:
 *      - Questions
 *     parameters:
 *      - in: path
 *        name: questionId
 *        schema:
 *              type: string
 *        required: true
 *        description: ID of the question to get answers
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: answers
 *         schema:
 *             $ref: '#/definitions/Answers'
 *       500:
 *          description: Internal server Error
 */
router.get('/:questionId/answers', (req, res) => {
    Question.findById(req.params.questionId)
        .exec()
        .then(question => {
            res.status(200).json(question.answers)
        })
        .catch(error => {
            res.status(500).json({error})
        })
})


/**
 * @swagger
 *
 * /api/questions/{questionId}/answers:
 *   post:
 *     description: Create an answer to the question
 *     tags:
 *      - Questions
 *     parameters:
 *      - in: path
 *        name: questionId
 *        schema:
 *              type: string
 *        required: true
 *        description: ID of the question to add answer to
 *      - name: answer
 *        description: answer object
 *        in:  body
 *        required: true
 *        type: object
 *        schema:
 *           $ref: '#/definitions/Answer'
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: new answer
 *         schema:
 *             $ref: '#/definitions/Answer'
 *       500:
 *          description: Internal server Error
 */
router.post('/:questionId/answers', (req, res) => {
    Question.findById(req.params.questionId)
        .exec()
        .then(question => {
            let answer = new Answer({
                answer: req.body.answer,
                user: req.body.user
            })
            Question.update({_id: req.params.questionId}, { $push: { answers: answer }})
                .exec()
                .then(result => {
                    res.status(200).json(answer)
                })
                .catch(error => {
                    res.status(500).json({error})
                })
        })
        .catch(error => {
            res.status(500).json({error})
        })
})


/**
 * @swagger
 *
 * /api/questions/answers/{answerId}:
 *   patch:
 *     description: Updates an Answer by id
 *     tags:
 *       - Questions
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: path
 *         name: answerId
 *         schema:
 *              type: string
 *         required: true
 *         description: ID of the answer to update
 *       - name: answer
 *         description: Answer object
 *         in:  body
 *         required: true
 *         type: object
 *         schema:
 *           $ref: '#/definitions/Answer'
 *     responses:
 *       200:
 *         description: updated answer
 *         schema:
 *           $ref: '#/definitions/Answer'
 *       404:
 *          description: answer not found
 *       500:
 *          description: Internal server Error
 */
router.patch('/answers/:answerId', (req, res) => {
    let id = req.params.answerId
    Question.findById(id)
        .exec()
        .then(answer => {
            if (answer == null) {
                res.status(404).json({message: "Question not found"})
            }
            let updatedAnswer = {}
            if (req.body.answer) {
                updatedAnswer.title = req.body.answer
                answer.title = req.body.answer
            }

            Answer.update({_id: id}, {$set: updatedAnswer})
                .exec()
                .then(result => {
                    res.status(200).json(answer)
                })
                .catch(error => {
                    res.status(500).json({error})
                })
        })
        .catch(error => {
            res.status(500).json({error})
        })
})


/**
 * @swagger
 *
 * /api/questions/answers/{answerId}:
 *   delete:
 *     description: Delete an Answer by id
 *     tags:
 *       - Questions
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: path
 *         name: answerId
 *         schema:
 *              type: string
 *         required: true
 *         description: ID of the answer to delete
 *     responses:
 *       200:
 *         description: deleted answer
 *         schema:
 *           $ref: '#/definitions/Answer'
 *       404:
 *          description: answer not found
 *       500:
 *         description: Internal server Error
 */
router.delete('/answers/:answerId', (req, res) => {
    let id = req.params.answerId
    Answer.findById(id)
        .exec()
        .then(answer => {
            if (answer == null) {
                res.status(404).json({message: "Answer not found"})
            }
            Answer.remove({_id: id})
                .then(result => {
                    res.status(200).json(answer)
                })
                .catch(error => {
                    res.status(500).json({error})
                })
        })
        .catch(error => {
            res.status(500).json({error})
        })
})



module.exports = router;