const router = require('express').Router()

const {Topic} = require('../../models')


/**
 * @swagger
 *
 * tags:
 *   - name: Topics
 *     description: blog topics
 *
 * definitions:
 *   Topic:
 *     type: object
 *     required:
 *       - _id
 *       - name
 *     properties:
 *       _id:
 *         type: string
 *       name:
 *         type: string
 *   Topics:
 *          type: array
 *          items:
 *              $ref: '#/definitions/Topic'
 */


/**
 * @swagger
 *
 * /api/topics:
 *   get:
 *     description: Get a list of topics
 *     tags:
 *       - Topics
 *     parameters:
 *      - in: header
 *        name: Authorization
 *        schema:
 *              type: string
 *              required: true
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Success
 *         schema:
 *              $ref: '#/definitions/Topics'
 *       500:
 *          description: Internal Server Error
 */
router.get('/', (req, res) => {
    Topic.find()
        .exec()
        .then(topics => {
            res.status(200).json(topics)
        })
        .catch(error => {
            res.status(500).json({error})
        })
})


/**
 * @swagger
 *
 * /api/topics:
 *   post:
 *     description: Creates a Topic
 *     tags:
 *       - Topics
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: header
 *         name: Authorization
 *         schema:
 *              type: string
 *              required: true
 *       - name: post
 *         description: Topic object
 *         in:  body
 *         required: true
 *         type: string
 *         schema:
 *           $ref: '#/definitions/Topic'
 *     responses:
 *       200:
 *         description: new topic
 *         schema:
 *           $ref: '#/definitions/Topic'
 */
router.post('/', (req, res) => {
    let topic = new Topic({
        name: req.body.name
    })
    topic.save()
        .then(user => {
            res.status(200).json(user)
        })
        .catch(error => {
            res.status(500).json({error})
        })
})


/**
 * @swagger
 *
 * /api/topics/{topicId}:
 *   get:
 *     description: Get a post by id
 *     tags:
 *      - Topics
 *     parameters:
 *      - in: header
 *        name: Authorization
 *        schema:
 *              type: string
 *              required: true
 *      - in: path
 *        name: topicId
 *        schema:
 *              type: string
 *        required: true
 *        description: ID of the topic to get
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Success
 *         schema:
 *             $ref: '#/definitions/Topic'
 *       404:
 *          description: topic not found
 *       500:
 *          description: Internal server Error
 */
router.get('/:topicId', (req, res) => {
    Topic.findById(req.params.topicId)
        .exec()
        .then(topic => {
            if (topic == null) {
                res.status(404).json({message: "Topic not found"})
            }
            res.status(200).json(topic)
        })
        .catch(error => {
            res.status(500).json({error})
        })
})


/**
 * @swagger
 *
 * /api/topics/{topicId}:
 *   patch:
 *     description: Updates a Topic by id
 *     tags:
 *       - Topics
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: header
 *         name: Authorization
 *         schema:
 *              type: string
 *              required: true
 *       - in: path
 *         name: topicId
 *         schema:
 *              type: string
 *         required: true
 *         description: ID of the topic to update
 *       - name: toic
 *         description: Topic object
 *         in:  body
 *         required: true
 *         type: string
 *         schema:
 *           $ref: '#/definitions/Topic'
 *     responses:
 *       200:
 *         description: updated topic
 *         schema:
 *           $ref: '#/definitions/Topic'
 *       404:
 *          description: topic not found
 *       500:
 *          description: Internal server Error
 */
router.patch('/:topicId', (req, res) => {
    let id = req.params.topicId
    let updatedTopic = {}
    if (req.body.name) { updatedTopic.name = req.body.name }
    Topic.findById(id)
        .exec()
        .then(topic => {
            if (topic == null) {
                res.status(404).json({message: "Topic not found"})
            }
            Topic.update({_id: id}, {$set: updatedTopic})
                .exec()
                .then(result => {
                    res.status(200).json(topic)
                })
                .catch(error => {
                    res.status(500).json({error})
                })
        })
        .catch(error => {
            res.status(500).json({error})
        })
})


/**
 * @swagger
 *
 * /api/topics/{topicId}:
 *   delete:
 *     description: Delete a Topic by id
 *     tags:
 *       - Topics
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: header
 *         name: Authorization
 *         schema:
 *              type: string
 *              required: true
 *       - in: path
 *         name: topicId
 *         schema:
 *              type: string
 *         required: true
 *         description: ID of the topic to delete
 *     responses:
 *       200:
 *         description: deleted topic
 *         schema:
 *           $ref: '#/definitions/Topic'
 *       404:
 *         description: topic not found
 *       500:
 *         description: Internal server Error
 */
router.delete('/:postId', (req, res) => {
    let id = req.params.topicId
    Topic.findById(id)
        .exec()
        .then(topic => {
            if (topic == null) {
                res.status(404).json({message: "Topic not found"})
            }
            Topic.remove({_id: id})
                .then(result => {
                    res.status(200).json(topic)
                })
                .catch(error => {
                    res.status(500).json({error})
                })
        })
        .catch(error => {
            res.status(500).json({error})
        })
})


module.exports = router;