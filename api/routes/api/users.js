const router = require('express').Router()

const UserController = require('../../controllers/UserController')


/**
 * @swagger
 *
 * tags:
 *   - name: Users
 *     description: users
 *
 * definitions:
 *   User:
 *     type: object
 *     required:
 *       - _id
 *       - username
 *       - email
 *       - password
 *       - dob
 *     properties:
 *       _id:
 *         type: string
 *       username:
 *         type: string
 *       email:
 *         type: string
 *       password:
 *         type: string
 *       dob:
 *         type: string
 *   Users:
 *          type: array
 *          items:
 *              $ref: '#/definitions/User'
 */


/**
 * @swagger
 *
 * /api/users:
 *   get:
 *     description: Get a list of users
 *     tags:
 *      - Users
 *     responses:
 *       200:
 *          description: users
 *          schema:
 *              $ref: '#/definitions/Users'
 *       500:
 *          description: Internal Server Error
 */
router.get('/', UserController.all)


/**
 * @swagger
 *
 * /api/users:
 *   post:
 *     description: Creates a User
 *     tags:
 *       - Users
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: user
 *         description: User object
 *         in:  body
 *         required: true
 *         type: object
 *         schema:
 *           $ref: '#/definitions/User'
 *     responses:
 *       200:
 *         description: new topic
 *         schema:
 *           $ref: '#/definitions/User'
 *       500:
 *          description: Internal server Error
 */
router.post('/', UserController.create)


/**
 * @swagger
 *
 * /api/users/{userId}:
 *   get:
 *     description: Get a user by id
 *     tags:
 *      - Users
 *     parameters:
 *      - in: path
 *        name: userId
 *        schema:
 *              type: string
 *        required: true
 *        description: ID of the user to get
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: user
 *         schema:
 *             $ref: '#/definitions/User'
 *       404:
 *          description: user not found
 *       500:
 *          description: Internal server Error
 */
router.get('/:userId', UserController.findById)

/**
 * @swagger
 *
 * /api/users/{userId}:
 *   patch:
 *     description: Updates a User by id
 *     tags:
 *       - Users
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: path
 *         name: userId
 *         schema:
 *              type: string
 *         required: true
 *         description: ID of the user to update
 *       - name: user
 *         description: User object
 *         in:  body
 *         required: true
 *         type: object
 *         schema:
 *           $ref: '#/definitions/User'
 *     responses:
 *       200:
 *         description: updated user
 *         schema:
 *           $ref: '#/definitions/User'
 *       404:
 *          description: user not found
 *       500:
 *          description: Internal server Error
 */
router.patch('/:userId', UserController.update)


/**
 * @swagger
 *
 * /api/users/{userId}:
 *   delete:
 *     description: Delete a User by id
 *     tags:
 *       - Users
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: path
 *         name: userId
 *         schema:
 *              type: string
 *         required: true
 *         description: ID of the user to delete
 *     responses:
 *       200:
 *         description: deleted user
 *         schema:
 *           $ref: '#/definitions/User'
 *       404:
 *          description: user not found
 *       500:
 *         description: Internal server Error
 */
router.delete('/:userId', UserController.delete)


module.exports = router;