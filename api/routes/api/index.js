const apiRoutes = require('express').Router()

// List all the routes here
apiRoutes.use('/posts', require('./posts'))
apiRoutes.use('/questions', require('./question'))
apiRoutes.use('/topics', require('./topics'))
apiRoutes.use('/users', require('./users'))

module.exports = apiRoutes